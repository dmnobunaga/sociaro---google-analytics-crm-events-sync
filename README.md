# Сервис синхронизации CRM с Google Analytics



### Google Analytics Classic
### URL: http://connect.sociaro.ai:2055/ga/ 
## Parameters

Request type:
`json/application`

Body

    {
        "tracker_id": "", // Tracker id from Google Analytics/Universal (required)
        "domain": "", // Your domain (required)
        "ip": "", // Visitor IP (optional)
        "cookie": "", // __utma cookie value (required)
        "page": "", // Page if page exist (optional)
        "category": "", (required) Example Signup
        "action": "", (required) Example Approve
        "label": "", (optional) 
    }
    

### Google Analytics Universal
### URL: http://connect.sociaro.ai:2055/ga/v1/ 
## Parameters

Request type:
`json/application`

Body

    {
        "tracker_id": "", // (required) Tracker id from Google Analytics/Universal 
        "ip": "", // Visitor IP (optional)
        "ga": "", // (required) _ga cookie value
        "page": "", // (optional) Page if page exist 
        "category": "", (required) Example Signup
        "action": "", (required) Example Approve
        "label": "", (optional) 
    }
    